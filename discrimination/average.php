<?php
$csv = array_map('str_getcsv', file('http://10.1.10.1/talk.csv'));
/* print_r($csv); */
$cnt=count($csv);

foreach($csv as $line){
  /* print "Line: ".$line[1]." : ".$line[2]." subjectivity: ".$line[3]." positivity: ".$line[4]."\n"; */
  $cnt_subjs+=$line[3];
  $cnt_pos+=$line[4];
}
/* print_r($ar_subjs); */
$str_subjs=$cnt_subjs/$cnt;
$str_pos=$cnt_pos/$cnt;
?>

<!DOCTYPE html>
<meta charset="utf-8">
<style>
  body{
  font-size:64px;
}
.chartsubj{
   /* background:#CCC; */
   /* height:24px; */
   /* width:320px; */
}
  .chartsubj div, .chartpos div {
  font: 20px sans-serif;
  background-color: steelblue;
  text-align: right;
  padding: 3px;
  margin: 1px;
  color: white;
  float:left;
}
.label{
  font: 20px sans-serif;
  float:left;
  margin-left:20px;
  width:180px;
}
.line{
  margin:80px 20px 60px 20px;
}
.clear{
  clear:both;
}
.bottom{
  font-size:14px;
  position:absolute;
  bottom:10px;
  left:0px;
  text-align:center;
  width:100%;
}
</style>
<div class="line">
<?php
  print "Global index on ".$cnt." sentences.";
?>
</div>
<div class="label">Subjectivity</div><div class="chartsubj"></div>
<br class="clear"/>
<div class="label">Positivity</div><div class="chartpos"></div>
<br class="clear"/>
<script src="http://d3js.org/d3.v3.min.js"></script>
<script>
<?php
  print "var datasubj = [".$str_subjs."];\n";
  print "var datapos = [".$str_pos."];\n";

?>
var x = d3.scale.linear()
  //  .domain([d3.min(data), d3.max(data)])
  .domain([-1, 1])
    .range([0, 520]);

d3.select(".chartsubj")
  .selectAll("div")
    .data(datasubj)
  .enter().append("div")
    .style("width", function(d) { return x(d) + "px"; })
    .text(function(d) { return d; });

d3.select(".chartpos")
  .selectAll("div")
    .data(datapos)
  .enter().append("div")
    .style("width", function(d) { return x(d) + "px"; })
    .text(function(d) { return d; });

</script>
<div class="bottom"><a href="read_ll.php">Sentence</a> <a href="average.php">Summary</a></div>