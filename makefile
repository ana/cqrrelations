# This make file takes any files ending with
# .md in the current folder
# and uses pandoc to turn them into HTML


allmd = $(wildcard *.md )

derivedhtml = $(patsubst %.md,%.html,$(allmd))

all : $(derivedhtml)

%.html: %.md
	pandoc $< -s --css=styles.css > $@

clean:
	rm -f $(derivedhtml)
