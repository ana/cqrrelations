import csv
import json
import numpy as np
import sys

CSV_PATH = sys.argv[1]
CSV_OUT =  sys.argv[2]
CORR_OUT =  sys.argv[3]
# Ignore columns that have more than 15 different values
MAX_N_TERMS = 15

r = csv.reader(open(CSV_PATH))
cols = r.next()

colterms = dict([(X,set()) for X in cols])
termcols = {}
#print colterms

def get_terms(colval):
    ret = []
    for values in colval.split("/"):
        values = values.replace("-", " to ")
        for term in values.split(" to"):
            ret.append(term.strip().lower())
    return ret

for line in r:
    for colname, colval in zip(cols, line):
        for term in get_terms(colval):
            colterms[colname].add(term)

# Figure out which fields may be comparable
comparable_columns = [colname for (colname,cterms) in colterms.items() if len(cterms) < MAX_N_TERMS]

# Make a set of all comparable terms
terms = set()
for colname in comparable_columns:
    terms.update(colterms[colname])
    # Make a backward link from terms to the column they come from
    for term in colterms[colname]:
        termcols.setdefault(term, set()).add(colname)

#print terms

# Load (hand-crafted) number-mapping JSON file
import json
term2number = json.load(open("term2number.json"))

comparable_columns = [X for X in comparable_columns if 
                      all([T in term2number for T in colterms[X]])]

#print comparable_columns

# Make a numerical version of the CSV
r = csv.reader(open(CSV_PATH))
cols = r.next()

arr = []

for line in r:
    row = []
    for colname, colval in zip(cols, line):
        if colname in comparable_columns:
            vals = []
            for term in get_terms(colval):
                vals.append(term2number[term])
            if None in vals:
                row.append(None)
            else:
                row.append(sum(vals) / float(len(vals)))
    arr.append(row)

# write out to a new CSV
w = csv.writer(open(CSV_OUT, 'w'))
w.writerow(['title'] + comparable_columns)

r = csv.reader(open(CSV_PATH))
cols = r.next()
for origline, line in zip(r, arr):
    w.writerow([origline[0]] + line)

correlations = np.zeros((len(comparable_columns), len(comparable_columns)))
nsamples = np.zeros((len(comparable_columns), len(comparable_columns)), dtype=int)

for idx1,colname1 in enumerate(comparable_columns):
    for idx2,colname2 in enumerate(comparable_columns):
        # Select rows that have values in column one *and* two
        intersection = np.array([(row[idx1], row[idx2]) for row in arr if (row[idx1] is not None) and (row[idx2] is not None)])
        #print intersection
        nsamples[idx1,idx2] = len(intersection)
        if len(intersection) > 0:
            correlations[idx1,idx2] = np.corrcoef(intersection[:,0], intersection[:,1])[0,1]

            print 'Correlation of', colname1, 'and', colname2, correlations[idx1,idx2]


json.dump(correlations.tolist(), open(CORR_OUT, 'w'))
