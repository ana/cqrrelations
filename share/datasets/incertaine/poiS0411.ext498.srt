﻿
We're gonna need to borrow one
of your computers.

00:31:45,002 --> 00:31:46,736
Coolly delivered sadistic
warning.

499
00:31:46,738 --> 00:31:48,271
Self-deprecating inquiry
into the time

500
00:31:48,273 --> 00:31:49,906
necessary to infiltrate system.

501
00:31:49,908 --> 00:31:52,209
Funny yet insightful retort.

502
00:31:52,211 --> 00:31:55,212
Mildly agitated declaration
of mission completion.

503
00:32:01,385 --> 00:32:03,620
Gentle exhortation
to further action.

504
00:32:08,893 --> 00:32:10,894
Overly affectionate greeting.

505
00:32:10,896 --> 00:32:12,996
Greeting.

506
00:32:12,998 --> 00:32:16,266
Transparent rationale
for conversation.

507
00:32:16,268 --> 00:32:18,568
Annoyed attempt
to deflect subtext.

508
00:32:18,570 --> 00:32:20,904
Overt come-on.

509
00:32:20,906 --> 00:32:25,308
Mildly embarrassed defensiveness
bordering on hostility.

510
00:32:26,277 --> 00:32:28,244
Playfully witty sign-off.

