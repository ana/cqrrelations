import mechanize
from mechanize import Browser 

#link collection on search page lens.org

#search: potato in full text + A01H5/00 in classification + granted patents -> 8093 results on 162 pages
#only first 20 pages give results
#search2: 'potato cultivar' in full text + A01H5/00 in classification -> 309 results on 7 pages

br = Browser()
#br.set_handle_robots(False)

for i in range(7):
#   url="http://www.lens.org/lens/search?q=%22potato+cultivar%22&v=table&p=" + str(i) + "&l=en&st=true&types=Granted+Patent&classIpcr=A01H5%2F00&n=50"
   url="http://www.lens.org/lens/search?classIpcr=A01H5%2F00&l=en&st=true&q=%22potato+cultivar%22&p=" + str(i) + "&n=50"
   searchresults_page=br.open(url).read()

   searchresults_file=open('./potato_patents/lenssearchresults2/lenssearch2_'+str(i), 'w')
   searchresults_file.write(searchresults_page)
   searchresults_file.close()


