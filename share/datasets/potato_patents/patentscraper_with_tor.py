#scraper for worldwide.espacenet.com/searchResults (European Patent Office)
#first save html of result page, and include path to file in source

#still some mistakes
#add tor to avoid anti-scraper behaviour

import requests
from lxml import html, etree
from time import sleep
import codecs
import os

import requests
import requesocks
 
 
session = requesocks.session()
 
session.proxies = {
'http': 'socks5://127.0.0.1:9050',
'https': 'socks5://127.0.0.1:9050'
} 

source="Espacenet_results_total.html" #path to saved page of search results
sourcefile=open(source, 'r')
rawsource=sourcefile.read()
sourcefile.close()
doc = html.fromstring(rawsource)
counter=0

rawlinks=doc.find_class('contentRowClass')

for raw in rawlinks:
   elements = raw.findall('td')
   patentid=elements[2].text_content().strip()
   CC=patentid[0:2]
   NRraw1=patentid[2:]
   NRraw2=NRraw1.split(u'\xa0')
   KCraw=NRraw2[1].split('(')
   KCraw2=KCraw[1].split(')')
   KC=KCraw2[0][0:2]
   print KC
   NR=NRraw2[0] + KC
   date=elements[3].text_content().strip().replace('-','')

   #url_description="http://worldwide.espacenet.com/publicationDetails/description?CC=" + CC + "&NR=" + NR + "&KC=" + KC + "&FT=D&ND=3&date=" + date + "&DB=EPODOC&locale=en_EP"
   #url_claims="http://worldwide.espacenet.com/publicationDetails/claims?CC=" + CC + "&NR=" + NR + "&KC=" + KC + "&FT=D&ND=3&date=" + date + "&DB=EPODOC&locale=en_EP"
   headd = {
   'Host' : 'worldwide.espacenet.com',
'User-Agent' : 'Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20140924 Firefox/24.0 Iceweasel/24.8.1',
'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
'Accept-Language' :  'en-US,en;q=0.5',
'Accept-Encoding' : 'gzip, deflate',
'Cookie' : 'currentUrl=http%3A%2F%2Fworldwide.espacenet.com%2FpublicationDetails%2Fdescription%3FKC%3DB1%26date%3D20141118%26FT%3DD%26locale%3Den_EP%26CC%3DUS%26DB%3DEPODOC%26NR%3D8889964B1%26ND%3D3; menuCurrentSearch=http%3A%2F%2Fworldwide.espacenet.com%2FsearchResults%3FDB%3Dworldwide.espacenet.com%26ST%3Dsingleline%26locale%3Den_EP%26query%3Dpotato; PGS=10; cpcops_settings=%7B%22display_tree%22%3Atrue%2C%22show-2000-series%22%3A%22state-1%22%7D; cart=%5B%5D; LastRequestedImagePage=1; LevelXLastSelectedDataSource=EPODOC; CAPTCHA_CHECK_RESULT=""; JSESSIONID=8E0011E4BBA3FD657756799A82B157FF.espacenet_levelx_prod_1; currentUrl=http%3A%2F%2Fworldwide.espacenet.com%2FsearchResults%3FST%3Dsingleline%26locale%3Den_EP%26submitted%3Dtrue%26DB%3Dworldwide.espacenet.com%26query%3Dpotato; org.springframework.web.servlet.i18n.CookieLocaleResolver.LOCALE=en_EP',
'Connection' : 'keep-alive'
   }
   
   url_description="http://worldwide.espacenet.com/publicationDetails/description"
   url_claims="http://worldwide.espacenet.com/publicationDetails/claims"
   payload = {'CC':CC, 'NR':NR, 'KC':KC, 'FT':'D', 'ND':'3', 'date':date, 'DB':'EPODOC', 'locale':'en_EP'}
   print url_description
   print payload
   page_description=session.get(url_description, params=payload, headers=headd)
   page_claims=session.get(url_claims, params=payload, headers=headd)

   url = 'http://icanhazip.com'
 
   resp = requests.get(url)
   print 'ip: {}'.format(resp.text.strip())
 
   resp = session.get(url)
   print 'tor ip: {}'.format(resp.text.strip()) 

   counter+=2
   if counter > 30 :
      os.system('printf "AUTHENTICATE \"DIOCANE\"\r\nSIGNAL NEWNYM\r\n" | nc 127.0.0.1 9051 -w 1') 	# this sets a nex exit node on the tor socket. it supposes you have DIOCANE as password in /etc/tor/torrc
      counter=0
   description_file=codecs.open('results_1/description_'+ NR, 'w', 'utf-8')
   description_file.write(page_description.text)
   description_file.close()
   claims_file=codecs.open('results_1/claims_'+ NR, 'w', 'utf-8')
   claims_file.write(page_claims.text)
   claims_file.close()


