from PIL import Image
import sys, json, os
from argparse import ArgumentParser


ap = ArgumentParser()
ap.add_argument("--format", default="json")
ap.add_argument("image", help="path to image")
args = ap.parse_args()

p = args.image
i = Image.open(p)
width, height = i.size
pixels = i.getdata()

if args.format == "json":
    x = 0
    data = {}
    data['path'] = os.path.basename(args.image)
    data['width'] = i.size[0]
    data['height'] = i.size[1]
    rows = []
    data['pixels'] = rows
    row = []
    for p in pixels:
        row.append(p)
        x += 1
        if x == width:
            rows.append(row)
            row = []
    print json.dumps(data)

elif args.format == "csv":
    import csv
    writer = csv.writer(sys.stdout)
    x, y = 0, 0
    for p in pixels:
        writer.writerow((x, y) + p)
        x += 1
        if x == width:
            y += 1
            x = 0
