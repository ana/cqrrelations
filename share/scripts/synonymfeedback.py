from pattern.en     import tag, sentiment, parsetree, wordnet
import random

random.seed()

#sentence="A correlation perceives objects behind the data, and these objects can have a sweet taste or a nasty smell. In return, the relation it creates may also be sticky or irritating."

sentence="Cqrrelation is poetry to the statistician. It is science to the dissident. It is detox to the data-addict. Cqrrelation is a typo-enhanced notion. It can also be pronounced as crummylation, crappylation, queerylation. These words try to hint at different shadowy elements of statistics and computing, and more specifically, at the problematic use of those disciplines to correlate big amounts of data and create models to determine reality and life based on parameters, criteria, numbers. A cqrrelation is a correlation with impurities, with missing, invisible, broken or suspicious data. Differently from a correlation, it does not pretend to be neutral, its relation with digital traces is not innocent. Allowing irony and speculation to contaminate empirical models and logical truths, a cqrrelation questions its capacity to produce models or truths, and happily undermines its own authority. It will also obstaculate the practice of a correlation, if deemed necessary. A cqrrelation is a correlation that is complicated by non-statistical constraints. We think computing and statistical techniques can be great tools for that, but there are many other tools to complement them; one should also be able to engage her own body, her own voice, touch objects and talk to people. A cqrrelation perceives objects behind the data, and these objects can have a sweet taste or a nasty smell. In return, the relation it creates may also be sticky or irritating. A cqrrelator breaks the statistician's oath by committing the sin of becoming emotional with data. And this generates even more data."

def replacemmm(ssss,index,recur):
    try:
        subs=random.choice(wordnet.synsets(ssss.words[index].string,pos=str(type)))
        if (len(subs.synonyms))<=1 or recur==1:
            subs=subs.hypernyms(recursive=False) #synset
            choicey=random.choice(subs) #choice of hyper synset
            subs=random.choice(choicey.hyponyms(recursive=False)) #choice of hypo of synset
            choicey=random.choice(subs.synonyms) #choice of synonyms=list of words
            if choicey not in ssss.string: # this stops word elongatinnngggg WELL NOT!
                ssss.words[index].string=choicey
                score=sentiment(ssss.string)[0]
                sss = parsetree(ssss.string, relations=True, lemmata=True)
                ssss=sss.sentences[0]
            else:
                replacemmm(ssss,index,1)
        else:
            if random.randint(0,10)==1:
                subs=subs.synonyms
            else:
                subs=random.choice(subs.antonym)
                subs=subs.synonyms #choice of synonyms=list of words
            choicey=random.choice(subs)
            if choicey not in ssss.string:
                ssss.words[index].string=choicey
                score=sentiment(ssss.string)[0]
                sss = parsetree(ssss.string, relations=True, lemmata=True)
                ssss=sss.sentences[0]
            else:
                replacemmm(ssss,index,1)
    except KeyboardInterrupt:
        raise
    except:
        return 0

# try on many sentences to up score of whole
sss = parsetree(sentence, relations=True, lemmata=True)
sssss=sss.sentences
score=0.0
listsofar=[]
for ssss in sssss:
    for x in range(1000):
        index=random.randint(0,ssss.stop-1)
        type= ssss.words[index].type #POS
        replacemmm(ssss,index,0)
print sss.string
