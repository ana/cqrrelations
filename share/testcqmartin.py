from pattern.web    import Twitter
from pattern.en     import tag, sentiment, parsetree, wordnet
from pattern.vector import KNN, count, Document, Model, HIERARCHICAL
import random

# LATENT SEMANTIC ANALYSIS. word2vec?, scikitlearn, question is comp. of documents

# twitter, knn = Twitter(), KNN()
# for i in range(1, 10):
#     for tweet in twitter.search('#win OR #fail', start=i, count=100):
#         s = tweet.text.lower()
#         p = '#win' in s and 'WIN' or 'FAIL'
#         v = tag(s)
#         v = [word for word, pos in v if pos == 'JJ'] # JJ = adjective

### type in sentence, score, synonyms and the re-score until hits peak
#sentence=raw_input("IN:")
#sentence="This police report documents the findings of the criminal investigation into an allegation made by Mohamed Al Fayed of conspiracy to murder the Princess of Wales and his son Dodi Al Fayed."
sentence="Correlation is poetry to the statistician, it is science to the dissident, it is detox to the data-addict."
#sentence="We think computing and statistical techniques can be great tools for that, but there are many other tools to complement them; one should also be able to engage her own body, her own voice, touch objects and talk to people."
#sentence="A correlation perceives objects behind the data, and these objects can have a sweet taste or a nasty smell. In return, the relation it creates may also be sticky or irritating."

# try on many sentences to up score of whole
sss = parsetree(sentence, relations=True, lemmata=True)
ssss=sss.sentences[0]

score=0.0
while score<0.9:
    index=random.randint(0,ssss.stop-1)
    type= ssss.words[index].type #POS
    try:
        subs=wordnet.synsets(ssss.words[index].string,pos=str(type))[0]
        # choose random synonym from this list 
#        print random.choice(subs.synonyms)
        # and put into sentence???
#        print len(subs.synonyms)
        if (len(subs.synonyms))==1:
            subs=(subs.hypernyms(recursive=True))[0]
            ssss.words[index].string=random.choice(subs.synonyms)
#            print ssss.words[index].string
        else:
            ssss.words[index].string=random.choice(subs.synonyms)
        score=sentiment(ssss.string)[0]
        print ssss.string, score
        sss = parsetree(ssss.string, relations=True, lemmata=True)
        ssss=sss.sentences[0]
    except:
        # do nothikng
        x=0

# subs
# must be NOUN, VERB,  or ADVERB

#print subs

#ss=sentiment(sentence)


#print repr(sss)
#score=ss[0]
#print score
# pos of sentence is in sss
# replace one word constrained on pos- how?????

# s = open("/root/diana/chapters/3_glass-crash/texts/shortpaget").read()

#ss = parsetree(s, relations=True, lemmata=True)
# #print repr(ss)

# # how to deal with just 1.0 // multiply by previous sentences?
# peaksent=0.0
# peaksentence="" 
# lastsentence=""
# for sentence in ss:
# #    for chunk in sentence.chunks:
# #        print chunk.type, [(w.string, w.type) for w in chunk.words]
#     sss=sentiment(sentence)
#     ssss=sentiment(lastsentence)
#     if peaksent<(sss[0]*ssss[0]):
#         peaksent=(sss[0]*ssss[0])
#         print peaksent
#         peaksentence=sentence
#     lastsentence=sentence
# print peaksentence.string, peaksent

# ss = open("/root/diana/chapters/3_glass-crash/generated/paget_exec001").read() 

# d1 = Document(s, name='wounds')
# d2 = Document(ss, name='paget')
# m=Model([d1, d2])
# m.reduce(2)

# #         v = count(v)
# #         if v:
# #             knn.train(v, type=p)

# # print knn.classify('sweet potato burger')
# # print knn.classify('stupid autocorrect')

# s = open("/root/diana/chapters/3_glass-crash/texts/wounds/crashwounds").read()
# ss = open("/root/diana/chapters/3_glass-crash/generated/paget_exec001").read() 

# d1 = Document(s, name='wounds')
# d2 = Document(ss, name='paget')
# m=Model([d1, d2])
# m.reduce(2)

# # for d in m.documents:
# #     print d.name
# #     for concept, w1 in m.lsa.vectors[d.id].items():
# #         for feature, w2 in m.lsa.concepts[concept].items():
# #             if w1 != 0 and w2 != 0:
# #                 print (feature, w1 * w2)
# v=m.lsa.transform(d1)
