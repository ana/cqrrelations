from pattern.db import Datasheet
from pattern.en import parse
from pattern.vector import Document, SLP
from sys import argv
from os import path
import glob

data = Datasheet.load(argv[1])

classifier = SLP.load('classifier.inhi')

i=0

counter = {
	'agreement': 0,
	'disagreement': 0,
	'missed': 0,
	'invented': 0,
}

qualifications = ['anarchistic', '\'neutral\'', 'paternalistic']

print """









		\033[4mBrussels, an afternoon\033[0m.














		\033[94m\033[1mThe Annotator\033[0m and \033[92m\033[1mThe Algorithm\033[0m











"""

for row in data.rows:
	line = row[0]
	annotator = int(row[1])
	i+=1

	algo = classifier.classify(Document(
		parse(
			line,
			tokenize=True,
			lemmata=True,
			tags=False,
			relations=False,
			chunks=True
		),
		stopwords=False
	))

	if algo == annotator:
		result = 'agree'
		counter['agreement'] += 1
		
	else:
		result = '\033[91mdisagree\033[0m'
		counter['disagreement'] += 1
		if annotator == 1:
			counter['missed'] += 1
		if algo == 1:
			counter['invented'] += 1
	print "---"
	print """
   {0},

		\033[92m\033[1mThe Algorithm:\033[0m
	It is {1}

		\033[94m\033[1mThe Annotator:\033[0m
	It is {2}
				( they {3} )""".format(i, qualifications[algo+1], qualifications[annotator+1], result)


print """
The \033[94m\033[1mThe Annotator\033[0m and \033[92m\033[1mThe Algorithm\033[0m discussed {0} cases. 

They agreed on {1} case(s) and disagreed on {2} case(s).

\033[92m\033[1mThe Algorithm\033[0m missed paternalism on {3} cases(s) but invented paternalism on {4} case(s).

\033[94m\033[1mThe Annotator\033[0m has an internal disagreement-rate of 20%, and has an  \033[92m\033[1mThe Algorithm\033[0m-\033[94m\033[1mThe Annotator\033[0m-disagreement-rate of {5}%

""".format(i, counter['agreement'], counter['disagreement'], counter['missed'], counter['invented'], (float(counter['disagreement'])/float(i))*100)
