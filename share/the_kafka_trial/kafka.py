#!/usr/bin/env python
#first input is inputfile
#second outputfile
#thierd argument is author

import sys, json
from pattern.en import parsetree, sentiment
from pattern.db import Datasheet

print sys.argv[1]
print sys.argv[2]

fileinput = open(sys.argv[1])

fileoutput=fileinput.read().decode('utf-8').replace(u'K.', u'K')

theoutput = parsetree(fileoutput, tokenize = True, tags = True, chunks = False, encoding = 'utf-8')

ds = Datasheet('sheet')
ds.append(["number", "sentence", "sentiment1","sentiment2","assesment"])
lists = []
for number, sentence in enumerate(theoutput):
	print sentence.string
	ds.append([number, sentence.string, sentiment(sentence.string)[0],sentiment(sentence.string)[1],sentiment(sentence.string).assessments])
	lists.append({"number": number, "sentence": sentence.string, "sentiment1": sentiment(sentence.string)[0], "sentiment2":sentiment(sentence.string)[1], "assessments":sentiment(sentence.string).assessments})

ds.save(sys.argv[2])
jsonlists = json.dumps(lists)

filejson = open('trial.json', 'w')
filejson.write(jsonlists)#.encode('utf-8'))
