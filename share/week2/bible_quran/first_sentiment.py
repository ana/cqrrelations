from pattern.web import URL, plaintext
from pattern.en import sentiment, parse
from pattern.db import Datasheet

ds = Datasheet()

for i in xrange(1,50):
	print i
	s = URL('http://av1611.com/kjbp/kjv-bible-text/Ge-%s.html' % i).download()
	s = plaintext(s)
	ds.append((s, 'True'))

for i in xrange(1,28):
	print i
	s = URL('http://av1611.com/kjbp/kjv-bible-text/Mt-%s.html' % i).download()
	s = plaintext(s)
	ds.append((s, 'False'))

ds.save('truth.csv')
print 'saved!'	

