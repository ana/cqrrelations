from pattern.web import URL, plaintext
from pattern.vector import Document, NB, KNN, SLP, SVM, POLYNOMIAL
from pattern.db import csv

classifier = SVM(kernel=POLYNOMIAL, degree=10)
for text, validity in csv('truth.csv'):
	print validity
	v = Document(text, type=validity, stopwords=True)
	classifier.train(v)

print classifier.classes

for i in xrange(1,20):
	s = URL('http://av1611.com/kjbp/kjv-bible-text/Joh-%s.html' % i).download()
	s = plaintext(s)
	print classifier.classify(Document(s))
	