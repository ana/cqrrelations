from pattern.web import Element
from pattern.en import parse
from pattern.vector import Document, SVM
from sys import argv
from os import path
import glob

print argv[1]
searchpath = path.join(argv[1], "*.xml")

genderClassifier = SVM()

textToClassifiy = "A kid who just came back from a 3-week trip to INDIA, where he didn't touch a basketball, played more than me. A kid who was playing his 2nd game on varsity level all season played more than me. I played a grand total of 3 minutes. My time keeps shrinking and I seriously do not know what I'm doing wrong. Especially since coach knew that I was hot on practice Thursday."
toClassify = parse(textToClassifiy, tokenize=True, lemata=True, tags=False, relations=False, chunks=False)

print "Looking for {0}".format(searchpath)

counter = 0

for filepath in glob.glob(searchpath):
	filename = path.split(filepath)[1]
	props = filename.split('.')
	gender = props[1]
	
	f = open(filepath, 'r')
	html = f.read()
	f.close()
	elements = Element(html)
	
	for post in elements('post'):
		text = post.content.strip()
		
		v = Document(
				parse(
					text,
					tokenize=True,
					lemmata=True,
					tags=False,
					relations=False,
					chunks=False
				),
				type=gender,
				stopwords=False
			)
		
		genderClassifier.train(v)

	print("{2}: Learned {0} -> {1}".format(filename, gender, counter))
	counter += 1
	
	if counter == 50:
		genderClassifier.save('./classifier.ihni')
		counter = 0
	