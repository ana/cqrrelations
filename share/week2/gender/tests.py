from pattern.web import Element
from pattern.en import parse
from pattern.vector import Document, SVM, SLP
from sys import argv
from os import path
import glob

searchpath = path.join(argv[1], "*.xml")

slpClassifier = SLP.load('SLPResults.ihni')
svmClassifier = SVM.load('classifier.ihni')

count = {
	'total': 0,
	'disagreement': {
		'slp': 0,
		'svm': 0,
	},
	'both_wrong': 0,
	'both_right': 0,
	'slp': {
		'right': 0,
		'wrong': 0
	},
	'svm': {
		'right': 0,
		'wrong': 0
	},
}

for filepath in glob.glob(searchpath):
	filename = path.split(filepath)[1]
	props = filename.split('.')
	gender = props[1]
	
	f = open(filepath, 'r')
	html = f.read()
	f.close()
	elements = Element(html)
	
	for post in elements('post'):
		text = post.content.strip()
		
		v = Document(
				parse(
					text,
					tokenize=True,
					lemmata=True,
					tags=False,
					relations=False,
					chunks=False
				)
			)
		
		count['total'] += 1
		
		slp = False
		svm = False
		
		if  slpClassifier.classify(v, discrete=True) <> gender:
			count['slp']['wrong'] += 1
		else:
			slp = True
			count['slp']['right'] += 1
		
		if  svmClassifier.classify(v, discrete=True) <> gender:
			count['svm']['wrong'] += 1
		else:
			svm = True
			count['svm']['right'] += 1
		
		 #Disagreement
		if svm <> slp:
			#SVM was right
			if svm:
				count['disagreement']['svm'] += 1
			#SLP was right
			else:
				count['disagreement']['slp'] += 1
		
		if slp == False and svm == False:
			count['both_wrong'] += 1
			
		if slp == True and svm == True:
			count['both_right'] += 1
		
		if count['total'] % 100 == 0:
			print "Tested {0}".format(count['total'])
			print "SLP"
			print "Right: {0}. Wrong: {1}".format(count['slp']['right'], count['slp']['wrong'])
			print "SVM"
			print "Right: {0}. Wrong: {1}".format(count['svm']['right'], count['svm']['wrong'])
			print "Disagreement: {0} SVM right: {1} SLP right: {2}".format(count['disagreement']['svm'] + count['disagreement']['slp'], count['disagreement']['svm'], count['disagreement']['slp'])
			print "Both right: {0}".format(count['both_right'])
			print "Both wrong: {0}".format(count['both_wrong'])