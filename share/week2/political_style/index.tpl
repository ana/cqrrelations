<html>
<head>
	<title>Check newspaper</title>
	<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
	<script>
	$(function(){
		$('#speech_form').submit(function(e){
			e.preventDefault()
			$.ajax({
				type: "POST",
				url: '/newspaper',
				data: $( "#speech_form" ).serialize(),
				success: function(data){
					// $( "#result" ).html( data.newspaper + '-ish' );
					$( "#result2" ).html( data.newspaper );
				}
			});
		});
		$('#loader_select').change(function(e){
			$.ajax({
				type: "POST",
				url: '/speech',
				data: {'speech': $(this).val() },
				success: function(data){
					$( "#speech" ).val( data.speech );
					$('#speech_form').submit();
				}
			});
		});
	});
	</script>
	<style>
	html{background:#99dcca;font-family: sans-serif}
	body{width:600px;margin:1em auto;background:#fff;padding:1em;border-radius:5px;}
	textarea{width:100%;height:10em;}
	#result{margin-top:1em;padding-top:1em;border-top:solid 1px #ccc;font-size:130%;font-weight:bold;}
	input{background:#567d73;color:#fff;margin-top:1em;padding:.5em;border:none;border-radius:5px;}
	#wrapper{position:relative;}
	#result2{
		position:absolute;bottom:0;left:0;width:100%;height:70%;
		-webkit-transform: rotate(-45deg); 
		-moz-transform: rotate(-45deg); 
		-o-transform: rotate(-45deg);
		-ms-transform: rotate(-45deg); 
		text-align:center;
		font-weight:bold;
		color:red;
		font-size:200%;
	}
	</style>
</head>
<body>
	<h1>Find Newspaper Style</h1>
	<div id='loader'>
		<label>Pick a text: <select id='loader_select'>
			<option value="none" selected></option>
			{% for speech in speeches %}
			<option value="{{speech}}">{{speech}}</option>
			{% endfor %}
		</select></label>
	</div>
	<div id='wrapper'>
	<form method='post' id='speech_form'>
		<textarea name='q' id='speech'></textarea>
		<input type='submit' value='Analyze'>
		<div id='result2'>
		</div>
	</form>
	</div>
	<div id='result'>

	</div>
</body>
</html>