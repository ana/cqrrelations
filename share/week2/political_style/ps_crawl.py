#!/usr/bin/env python
import feedparser
from urllib import urlopen
from pattern.web import DOM, plaintext, URL, cache
from pattern.db import Datasheet, csv

cache._set_path('tmp')
# print cache._get_path()

# exit(1)

feeds = {
	'AD': 			{'url' : 'http://www.ad.nl/rss.xml', 				'selector': '#detail_content p'},
	'Volkskrant': 	{'url' : 'http://www.volkskrant.nl/nieuws/rss.xml', 'selector': '.article__wrapper p'},
	'Telegraaf': 	{'url' : 'http://www.telegraaf.nl/rss/', 			'selector': '#artikelKolom p'},
	'NRC': 			{'url' : 'http://www.nrc.nl/rss.php', 				'selector': '#broodtekst p'},
	'Sp!ts': 		{'url' : 'http://www.spitsnieuws.nl/rss.xml', 		'selector': '.content .field-items p'},
	'Trouw': 		{'url' : 'http://www.trouw.nl/rss.xml', 			'selector': '.art_cntr1_det p'},
}

ds = Datasheet()

for title, content, newspaper in csv('newspaper_items.csv'):
	ds.append((title, content, newspaper))

start_count = len(ds)
print 'Loaded previous version', start_count

# collect feeds
for newspaper, feed in feeds.iteritems():
	print newspaper, feed['url']
	# download ans loop over entries
	for entry in feedparser.parse(feed['url']).entries:
		# No link? Skip
		if not entry.link:
			continue


		# skip if it already exists
		exists = False
		for t,c,n in ds:
			if t == entry.title and n == newspaper:
				exists = True
				break

		if exists:
			print '\t\tALREADY THERE, SKIP'
			continue

		print '\t',entry.link

		# download page and parse content using selector
		# html = urlopen(entry.link).read()
		html = URL(entry.link).download()
		paragraphs = DOM(html).body(feed['selector'])
		content = "";
		for p in paragraphs:
			content += "\n" + p.content

		content = plaintext(content)
		# in the end no content? Don't save
		if not len(content):
			continue


		ds.append((entry.title, content, newspaper))

print 'ADDED', len(ds), 'ITEMS (', len(ds)-start_count, 'NEW)',
ds.save('newspaper_items.csv')