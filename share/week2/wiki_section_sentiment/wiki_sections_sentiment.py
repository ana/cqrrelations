#!/usr/bin/python
# -*- coding: utf-8 -*-

# how subjective is the wikipedia article for Neutrality?

my_article = "Mussolini"

from pattern.web import Wikipedia
from pattern.en import sentiment

article = Wikipedia().search(my_article)

print "Wikipedia article: " + my_article + "\n" 
for section in article.sections:
	secTitle = section.title
	#print section.content
	secSubj = sentiment(section.content)[1]
	secPos = sentiment(section.content)[0]

	print "'" + secTitle + "' is " + str(secSubj) + " subjective and " + str(secPos) + " positive."
print '\n'
