from pattern.vector import Document
import sys, json

p = sys.argv[1]

with open(p) as f:
    history = json.load(f)
    revs = history['revisions']
    for rev in revs:
        for line in rev['content_diff']:
            doc = Document(line)
            print doc
