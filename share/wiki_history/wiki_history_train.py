from pattern.vector import count
from pattern.vector import chngrams
from pattern.vector import NB, SVM, SLP
from pattern.vector import kfoldcv

from pattern.text import tokenize



import json
data = []

def vector(s):
    # A combination of techniques discussed in classification1.py.
    # Note the dictionary's update() method.
    # It takes another dictionary and adds its keys/values.
    return s
    # polarity, subjectivity = sentiment(s, language="nl")
    # v = {}
    # s = tokenize(s)
    # s = "\n".join(s)
    # print "s", s
    # v.update(chngrams(s.lower(), n=2))
    # v.update(chngrams(s.lower(), n=3))
    # v.update(chngrams(s.lower(), n=4))
    # v.update(count(s.lower().split(" ")))
    # v["<polarity>"] = polarity + 1 # non-negative number
    # v["<subjectivity>"] = subjectivity
    # return v

with open("terror.100.json", "r") as f:
    history = json.load(f)
    revs = history['revisions']
    print len(revs)

    for row in revs:
        for line in row['content_diff']:
            if line.startswith("-"):
                data.append([vector(line), "del"])
            elif line.startswith("+"):
                data.append([vector(line), "add"])

from pattern.vector import KNN
[accuracy, precision, recall, F1score, deviation] = kfoldcv(KNN, data, folds=10)
print "Accuracy: "+str(accuracy)
print "Precision: "+str(precision)
print "Recall: "+str(recall)
print "F-score: "+str(F1score)
